/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabajofinalprogramacion;

import java.io.Serializable;

/**
 *
 * @author portatil
 */
public class Ropa extends Productos implements Serializable{

    private  String talla;
    private  String color;
    
   

    public Ropa(String nombre, float precio, int cantidad,String talla, String color) {
        
        super(nombre, precio, cantidad);
        this.talla = talla;
        this.color = color;
    }
  
    @Override
    public String toString() {
        return  super.toString() +"\n\n\t # Ropa # " 
                + "\n♦ Talla: " + talla
                + "\n♦ Color: " + color;
    }
    
    // Comienzan los GET y SET
    /**
     * @return the talla
     */
    public String getTalla() {
        return talla;
    }

    /**
     * @param talla the talla to set
     */
    public void setTalla(String talla) {
        this.talla = talla;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

}
