/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabajofinalprogramacion;

//import java.io.IOException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author mrodriguezmolares
 */
public class inicio {

    private Scanner producto = new Scanner(System.in);
    private Scanner menu = new Scanner(System.in);
    private static HashMap<String, Productos> referencia = new HashMap<String, Productos>();
    private static BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));
    Scanner datos = new Scanner(System.in);
    private static Complementos compl;
    private static Ropa rop;
    private static Productos pro;
    public static String nombre;
    public static String caracteristicas;
    public static int cantidad;
    public static String talla, color, genero;
    public static float precio;
    private static String opcion;
    public static String codigoProducto;

    public void inicio() {

        while (true) {
            try {
                //declaramos la variable para recoger opciones del switch
                do {

                    System.out.println("\n\n¿Que operación quieres realizar?");
                    System.out.println("1.-Alta de producto");
                    System.out.println("2.-Baja de producto");
                    System.out.println("3.-Modificación de producto");
                    System.out.println("4.-Venta de producto");
                    System.out.println("5.-Listado de productos");
                    System.out.println("6.-Serializar productos");
                    System.out.println("7.-Salir");
                    System.out.println("Elija una opción:\n ");

                    opcion = entrada.readLine();

                } while (!opcion.equals("1") && !opcion.equals("2") && !opcion.equals("3")
                        && !opcion.equals("4") && !opcion.equals("5") && !opcion.equals("6")
                        && !opcion.equals("7"));
                // alta
                switch (opcion) {
                    case "1":
                        alta();
                        break;
                    case "2":
                        baja();
                        break;
                    case "3":
                        modificar();
                        break;
                    case "4":
                        venta();
                        break;
                    case "5":
                        lista();
                        break;
                    case "6":
                        serializa();
                        break;
                    case "7":
                        System.out.println("\nHa elegido salir. Que tenga un buen día");
                        System.exit(0);
                        break;
                }
            } catch (NumberFormatException nfe) {
                System.out.println("\nError de formato numérico: " + nfe.toString());
            } catch (IOException ioe) {
                System.out.println("\nError de entrada/salida: " + ioe.toString());
            } catch (Exception e) {
                // Captura cualquier otra excepción que pueda ocurrir.
                System.out.println(e.toString());
            }
        }
    }

    public static void alta() throws IOException {

        // Damos de alta un producto.
        System.out.println("\n -- Alta de producto -- ");
        System.out.println("Introduzca el nombre del producto: ");
        nombre = entrada.readLine();

        System.out.println("Introduzca la cantidad: ");
        cantidad = Integer.parseInt(entrada.readLine());

        System.out.println("Introduzca el precio: ");
        precio = Float.parseFloat(entrada.readLine());

        System.out.println("Introduzca las caracteristicas: ");
        caracteristicas = entrada.readLine();

        do {
            System.out.println("Introduzca tipo producto (1-Ropa, 2-Complementos): ");
            opcion = entrada.readLine();
        } while (!opcion.equals("1") && !opcion.equals("2"));
        if (opcion.equals("1")) {
            System.out.println("Trabajando en la petición, espere por favor... \n\n");

            System.out.println("Introduzca la talla: ");
            talla = entrada.readLine();
            System.out.println("Introduzca el color: ");
            color = entrada.readLine();

            rop = new Ropa(nombre, precio, cantidad, talla, color);
            codigoProducto = rop.getCodigo();
            referencia.put(codigoProducto, rop);

            System.out.println("\nRopa dada de alta: " + rop.toString());

        } else if (opcion.equals("2")) {
            System.out.println("Trabajando en la petición, espere por favor...\n\n ");

            //Datos de complementos añadidos:
            System.out.println("Elija una de las opciones:\n"
                    + "1-Mujer\n"
                    + "2-Hombre\n"
                    + "3-Cancelar Alta\n");
            genero = entrada.readLine();

            if (genero.equals("1")) {
                genero = "Mujer";
            } else if (genero.equals("2")) {
                genero = "Hombre";
            } else if (genero.equals("3")) {
                System.exit(0);
            }
            compl = new Complementos(nombre, precio, cantidad, genero);
            codigoProducto = compl.getCodigo();
            referencia.put(codigoProducto, compl);

            System.out.println("\nComplemento dado de alta: " + compl.toString());

        }
    }

    public static void baja() throws IOException {

        System.out.println("introduce el codigo del producto que quieres dar de baja: ");
        codigoProducto = entrada.readLine();
        //verifico que existe el codigo del producto
        referencia.get(codigoProducto);
        //se ve si esta dado de alta y si esta lo borra
        if (codigoProducto != null) {
            referencia.remove(codigoProducto);
        } else {
            System.out.println("El producto que quieres dar de baja no exixste");
            JOptionPane.showInputDialog("El producto que quieres dar de baja no exixste");
        }

    }

    public static void modificar() throws IOException {

        System.out.println("Introduce el codigo del producto que quieres modificar: ");
        codigoProducto = entrada.readLine();
        //verifico que existe el codigo del producto
        referencia.get(codigoProducto);
        //se ve si esta dado de alta y si esta lo modifico
        if (codigoProducto != null) {
            //pido  que quiere modificar
            System.out.println("Elige campo a modificar: ");
            System.out.println("1.-Nombre del producto");
            System.out.println("2.-Cantidad del producto");
            System.out.println("3.-Precio del producto");
            System.out.println("4.-Caracteristicas del producto");
            System.out.println("5.-Salir");

            opcion = entrada.readLine();
            switch (opcion) {

                case "1":
                    System.out.println("Introduzca el nombre del producto: ");
                    nombre = entrada.readLine();
                    break;
                case "2":
                    System.out.println("Introduzca la cantidad: ");
                    cantidad = Integer.parseInt(entrada.readLine());
                    break;
                case "3":
                    System.out.println("Introduzca el precio: ");
                    precio = Float.parseFloat(entrada.readLine());
                    break;
                case "4":
                    System.out.println("Introduzca las caracteristicas: ");
                    caracteristicas = entrada.readLine();
                    break;
                case "5":
                    System.out.println("Ha elejido salir, adios");
                    break;

                default:
                    System.out.println("Ha elejido una opción incorrecta");
                    break;
            }
        } else {
            System.out.println("El producto que quieres modificar no exixste");
        }

    }

    private static void venta() throws IOException {

        System.out.println("\n -- Venta de producto -- ");

        System.out.println("Introduzca el código del producto: ");
        codigoProducto = entrada.readLine();

        System.out.println("Introduzca la cantidad: ");
        cantidad = Integer.parseInt(entrada.readLine());
        pro = (Productos) referencia.get(codigoProducto);

        //Comprobamos que no esté el campo a nulo o vacio.
        if (pro != null) {
            if (pro.restaCantidad(cantidad)) {
                System.out.println("\nPrecio: " + pro.getPrecio() * cantidad);

                // Sale el producto con el nombre y datos comprados.
                System.out.println("Producto comprado: " + pro.toString());

                //Generamos la factura del producto.
                generarFactura(pro, cantidad);
            }

        } else {
            System.out.println(" Error: - Producto no encontrado - ");
        }
    }

    // Generamos factura imprimiendo en el fichero.
    private static void generarFactura(Productos pro, int cantidad) throws IOException {
        PrintWriter salida = new PrintWriter(new BufferedWriter(new FileWriter("factura.txt")));
        salida.println("----------------------- Factura Venta -------------------------");
        salida.println("\n");
        salida.println(pro.toString());
        salida.println("\n");
        salida.println("\n");
        salida.println("------------------------ Datos de la Factura -------------------------");
        salida.println("\n");
        salida.println("Cantidad Comprada: " + cantidad);
        salida.println("Precio: " + pro.getPrecio() * cantidad);
        salida.close();
    }

    // serializa
    static void serializa() throws IOException {
        System.out.println("\n -- Serializar productos -- ");
        System.out.println("Estás seguro (S/N):");
        String res = entrada.readLine().toUpperCase();
        if (res.equals("S")) {
            //Serialización de la tabla hash de productos y el contador
            FileOutputStream fosPro = new FileOutputStream("productos.dat");
            ObjectOutputStream oosPro = new ObjectOutputStream(fosPro);
            Integer cont = new Integer(Productos.getContador());
            oosPro.writeObject(cont);
            fosPro.close();
            System.out.println("\n Productos serializados...");
        }
    }

    private static void lista() throws IOException {
        System.out.println("\n -- Listar productos -- ");

        // Comparador
        Comparator PreProComp = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {

                Productos pro1 = (Productos) o1;
                Productos pro2 = (Productos) o2;
                Double p1 = new Double(pro1.getPrecio());
                Double p2 = new Double(pro2.getPrecio());
                return p1.compareTo(p2);

            }
        };
        //Convertimos la tabla hash en un array de objetos
        Object productosA[] = referencia.values().toArray();
        //Ordenamos los objetos del array por el atributo Precio
        Arrays.sort(productosA, PreProComp);

        //Presentamos la información
        System.out.println("\nProductos ordenadas por Precio:");
        for (int i = 0; i < productosA.length; i++) {
            System.out.println(productosA[i].toString());
        }
    }
}
