/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabajofinalprogramacion;

import java.io.Serializable;

public class Productos implements Serializable {

    private final String codigoProducto;
    private String nombre;
    private float precio;
    private int cantidad;
    private static int contador = 0;
    
 /*   public Productos(){
        
    }*/

    public Productos(String nombre, float precio, int cantidad) {
        this.nombre = nombre;
        this.precio = precio;
        this.cantidad = cantidad;
        this.codigoProducto = Integer.toString(getContador());
    }

    public static int getContador() {
        return contador++;
    }

    public static void setContador(int contador) {
        Productos.contador = contador;
    }

    public String getCodigo() {
        return this.codigoProducto;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPrecio() {
        return this.precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getCantidad() {
        return this.cantidad;
    }

    public void sumaCantidad(int cantidad) {
        this.cantidad += cantidad;
    }

    public boolean restaCantidad(int cantidad) {
        if (this.cantidad < cantidad) {
            System.out.println("No hay suficiente cantidad...");
            return false;
        } else if (this.cantidad == 1) {
            System.out.println("\tATENCION!! \nEs la ultima unidad del producto.");
            return false;
        } else {
            this.cantidad -= cantidad;
            return true;
        }
    }

    public String toString() {
        
        return "\n • Codigo del producto: " + codigoProducto
                + "\n • Nombre del producto: " + nombre
                + "\n • Precio: " + precio
                + "\n • Cantidad comprada: " + cantidad;
    }

}
