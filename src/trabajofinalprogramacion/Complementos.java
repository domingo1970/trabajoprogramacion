/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabajofinalprogramacion;



// Clase complementos extienda de productos.
public class Complementos extends Productos {
    
    private String genero;

    public Complementos(String nombre, float precio, int cantidad,String genero) {
        super(nombre, precio, cantidad);
        this.genero=genero;
    }
             
      //Get y Set
  
    /**
     * @return the genero
     */
    public String getGenero() {
        return genero;
    }

    /**
     * @param genero the genero to set
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }
        
    @Override
    public String toString() {
        return  super.toString() +"\n\t # Complemento # "
                + "\n♦ Genero: " + getGenero();
    }

  

}
